package com.tedneward.example;

import java.beans.*;
import java.util.*;

public class Person implements Comparable<Person>{
  private int age;
  private String name;
  private double salary;
  private String ssn;
  private int count = 0;
  private boolean propertyChangeFired = false;
  
  public Person() {
    this("", 0, 0.0d);
  }
  
  public Person(String n, int a, double s) {
    name = n;
    age = a;
    salary = s;
    count++;
    ssn = "";
  }
  
  @Override
  public int compareTo(Person other) {
  	return ((int)(this.salary - other.salary) * -1);
  }
  
  public static class AgeComparator implements Comparator<Person>{
	  public int compare(Person p1, Person p2){
		  return p1.getAge() - p2.getAge();
	  }
  }

  public static ArrayList<Person> getNewardFamily(){
	  ArrayList<Person> newards = new ArrayList<Person>();
	  newards.add(new Person("Ted", 41, 250000));
	  newards.add(new Person("Charlotte", 43, 150000));
	  newards.add(new Person("Michael", 22, 10000));
	  newards.add(new Person("Matthew", 15, 0));
	  return newards;
  }
  
  public int getAge() {
    return age;
  }
  public void setAge(int value){
	  if(value < 0){throw new IllegalArgumentException("impossibru!!");} else 
	  {
		  int old = age;
		  age = value;
		  this.pcs.firePropertyChange(new PropertyChangeEvent(this, "age", old, value));
	    propertyChangeFired = true;
    }
  }
  
  public String getName() {
    return name;
  }
  public void setName(String value){
	  if (value == null){ throw new IllegalArgumentException("how did you do that?");} else
	  { 
		  String old = name;
		  name = value;
		  this.pcs.firePropertyChange(new PropertyChangeEvent(this, "name", old, value));
      propertyChangeFired = true;
	  }
  }
  
  public double getSalary() {
    return salary;
  }
  public void setSalary(double value){
	  double old = salary;
	  salary = value;
	  this.pcs.firePropertyChange(new PropertyChangeEvent(this, "salary", old, value));
    propertyChangeFired = true;
  }
  
  public String getSSN() {
    return ssn;
  }
  public void setSSN(String value) {
    String old = ssn;
    ssn = value;
    this.pcs.firePropertyChange(new PropertyChangeEvent(this, "ssn", old, value));
    propertyChangeFired = true;
  }
  public boolean getPropertyChangeFired() {
    return propertyChangeFired;
  }

  public int count(){
	  return count;
  }
  
  public double calculateBonus() {
    return salary * 1.10;
  }
  
  public String becomeJudge() {
    return "The Honorable " + name;
  }
  
  public int timeWarp() {
    return age + 10;
  }
  
  @Override
  public boolean equals(Object p) {
    if(p instanceof Person){
      Person other = (Person)p;
      return (this.name.equals(other.name) && this.age == other.age);
    }
    return false;
  }

  public String toString() {
    return "[Person name:" + name + " age:" + age + " salary:" + salary + "]";
  }

  // PropertyChangeListener support; you shouldn't need to change any of
  // these two methods or the field
  //
  private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
  public void addPropertyChangeListener(PropertyChangeListener listener) {
      this.pcs.addPropertyChangeListener(listener);
  }
  public void removePropertyChangeListener(PropertyChangeListener listener) {
      this.pcs.removePropertyChangeListener(listener);
  }

}
